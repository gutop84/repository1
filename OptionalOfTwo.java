class Pair<T,S> {
	private final T value1 ;
	private final S value2 ;

	private Pair ( T v1,S v2 ) {
		if(v1!=null)this . value1 = Objects . requireNonNull ( v1 );
		else this . value1=null;
		if(v2!=null)this . value2 = Objects . requireNonNull ( v2 );
		else this . value2=null;
		}

	public boolean equals(Object obj)
	{
		if (this == obj)return true;
		if(obj==null)return false;
		if (getClass() != obj.getClass())return false;
		if (this.value1==null)
			if(((Pair)obj).value1!=null)return false;
			else
			if (this.value2==null)
				if(((Pair)obj).value2!=null)return false;
				else return true;
			else
				return ((this.value2.equals(((Pair)obj).value2)));
		else
		if (this.value2==null)
			if(((Pair)obj).value2!=null)return false;
			else
				return ((this.value1.equals(((Pair)obj).value1)));
		else
			return ((this.value1.equals( ((Pair)obj).value1)))&&((this.value2.equals(((Pair)obj).value2)));
	}
	
	public int hashCode()
	{
		int ii,iii;
		if(value1!=null)ii=value1.hashCode();
		else ii=0;
		if(value2!=null)iii=value2.hashCode();
		else iii=0;
		return ii+iii;
	}
	
	public T getFirst() {
		// TODO Auto-generated method stub
		return this.value1;

	}	
	public S getSecond() {
		// TODO Auto-generated method stub
		return this.value2;
	}

	public static<T,S> Pair<T,S> of(T tt, S ss) {
		// TODO Auto-generated method stub
		return new Pair<>(tt,ss);
	}
}


