import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {
public static void main(String[] args) {	
	BufferedReader bufReader = new BufferedReader(new InputStreamReader(System.in, Charset.forName("UTF-8")));
	Scanner in = new Scanner(bufReader);
	Stream.Builder<String> builder = Stream.builder();
	String str;
	while(in.hasNext()) {
		str=in.next().toLowerCase().toString();
		if(str.matches("(.*)-(.*)"))
		{
		String[] splited = str.split("\\-");
		for(int i=0;i<splited.length;i++) {
		builder.add(splited[i]);
		}
		}
		else
			builder.add(str);
		
	}

	Stream<String> streamGenerated=builder.add("\n").build();
	Predicate<? super String> prdk = (o1) -> 
	{
		if(o1.matches("[a-zA-Zа-яА-Я0-9']+")) 
		{
			return true;
		}
		else
		{
			return false;
		}
	};
	
	Map<String,Integer> num=new HashMap<String,Integer>();
	
	Comparator<String> ravno = (o1, o2) -> 
	{
		if(num.get(o1)==num.get(o2))
			return (o1.compareTo(o2));
		else
			return -(num.get(o1)-num.get(o2));
	};
    
	Predicate<? super String> redUse = (o1) -> 
	{
		int i;
		if(num.get(o1)>1) 
		{
			i=num.get(o1)-1;
			num.put(o1,i);
			return false;
		}
		else
		{
			return true;
		}
	};
    
    List<String> list  = streamGenerated
    		.map((s)->(s.contains(".")||s.contains(",")||s.contains("!")||s.contains("?")?s.replaceAll("[,.!?]", ""):s))
    		.filter(prdk)
    		.peek(
    				(s)->{
    					int i;
    					if(num.containsKey(s)) {
    						i=num.get(s)+1;
    						num.put(s,i);
    					}
    					else
    						num.put(s,1);
    				}
    				)
    		.sorted(ravno)
    		.filter(redUse)
    		.limit(10)
    		.collect(Collectors.toList());

    for(int i=0;i<Integer.min(10,list.size());i++)
    	 System.out.println(list.get(i));
}

}