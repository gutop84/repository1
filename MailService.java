public static class MailMessage implements Sendable<String>{
	String from;
	String to;
	String content;
	public MailMessage(String f,String t,String c){
		this.from=f;
		this.to=t;
		this.content=c;
	}
	public String getFrom() {
		return from;
	}
	public String getTo() {
		return to;
	}
	public String getContent() {
		return content;
	}
}

public static class Salary implements Sendable<Integer>{
	String from;
	String to;
	Integer content;
	public Salary(String f,String t,int c){
		this.from=f;
		this.to=t;
		this.content=Integer.valueOf(c);
	}
	public String getFrom() {
		return from;
	}
	public String getTo() {
		return to;
	}
	public Integer getContent() {
		return content;
	}
}

public interface Sendable<T>{
	String getFrom();
	String getTo();
	T getContent();
}


public static class MailService <T> implements Consumer<Sendable<T>>{
	Map<String,List<T>> mailBox;

	public MailService (){
		this.mailBox =new HashMap<String,List<T>>(){
            @Override
            public List<T> get(Object key) {
                return super.getOrDefault(key, new LinkedList<T>());
            }
        };

	}

@Override
public void accept(Sendable<T> t) {
	if(mailBox.containsKey(t.getTo())) {
		List<T> buf=mailBox.get(t.getTo());
		buf.add(t.getContent());
		mailBox.put(t.getTo(), buf);
	}
	else {
		List<T> buf=new ArrayList<>();
		buf.add(t.getContent());
		mailBox.put(t.getTo(), buf);
	}	
}
	
public Map<String, List<T>> getMailBox() {
    return mailBox;
}
    
}